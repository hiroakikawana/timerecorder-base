class TimerecordsController < ApplicationController
  protect_from_forgery :except => [:create, :destroy]
  before_action :set_timerecord, only: [:show, :edit, :update, :destroy]

  # GET /timerecords
  def index
    @timerecords = Timerecord.all
    render json: @timerecords
  end

  # GET /timerecords/1
  def show
    render json: @timerecord
  end

  # POST /timerecords
  def create
    @timerecord = Timerecord.new(timerecord_params)

    if @timerecord.save
      render json: @timerecord, status: :created, location: @timerecord
    else
      render json: @timerecord.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /timerecords/1
  def update
    if @timerecord.update(timerecord_params)
      render json: @timerecord
    else
      render json: @timerecord.errors, status: :unprocessable_entity
    end
  end

  # DELETE /timerecords/1
  def destroy
    @timerecord.destroy
  end

  private
    def set_timerecord
      @timerecord = Timerecord.find(params[:id])
    end

    def timerecord_params
      params.require(:timerecord).permit(:idm, :punch_class, :punch_date, :punch_time, :modify_time, :export_date)
    end
end
