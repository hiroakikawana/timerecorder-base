ActiveAdmin.register Employee do
  permit_params :employee_code, :fullname, cards_attributes: [:employee_id, :idm, :start_date, :end_date, :_destroy, :id]
  
  form do |f|
    f.inputs '従業員' do
      f.input :employee_code
      f.input :fullname
      f.has_many :cards, allow_destroy: true, heading: false, new_record: true do |ab|
        ab.input :id,
        label: 'IDm',
        as: :select,
        collection: Card.all.map { |a| [a.idm, a.id] }
        ab.input :start_date
        ab.input :end_date
      end
      f.actions
    end
  end
end
