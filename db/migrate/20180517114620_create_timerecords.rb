class CreateTimerecords < ActiveRecord::Migration[5.1]
  def change
    create_table :timerecords do |t|
      t.string :idm
      t.integer :punch_class
      t.date :punch_date
      t.datetime :punch_time
      t.datetime :modify_time
      t.date :export_date

      t.timestamps
    end
    add_index :timerecords, [:idm, :punch_class, :punch_date], unique: true
  end
end
