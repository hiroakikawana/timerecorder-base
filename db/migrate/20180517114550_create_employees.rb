class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :employee_code
      t.string :fullname

      t.timestamps
    end
  end
end
