class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards do |t|
      t.references :employee, foreign_key: true
      t.string :idm
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
